package com.midterm.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.midterm.model.Question;

public class QuestionDao {
	private Connection connection;
	private PreparedStatement preparedStatement;
	private ResultSet resultSet;

	public QuestionDao(Connection connection) {
		this.connection = connection;
	}

	public Question getById(int id) {
		String sql = "select * from question where id = ?";
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, id);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				return new Question(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3),
						new UserDao(this.connection).getById(resultSet.getInt(4)));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public Question getByUserId(int userid) {
		String sql = "select * from question where userid = ?";
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, userid);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				return new Question(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3),
						new UserDao(this.connection).getById(resultSet.getInt(6)));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public Boolean remove(Question question) {
		String sql = "delete from question where id = ?";
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, question.getId());
			return preparedStatement.executeUpdate() < 1 ? Boolean.FALSE : Boolean.TRUE;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public Boolean add(Question question) {
		String sql = "insert into question (id, question, response, userid) VALUES (?, ?, ?, ?)";
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, 0);
			preparedStatement.setString(2, question.getQuestion());
			preparedStatement.setString(3, null);
			preparedStatement.setInt(4, question.getUser().getId());
			return preparedStatement.executeUpdate() < 1 ? Boolean.FALSE : Boolean.TRUE;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public Boolean update(Question question) {
		String sql = "update question set question = ?, response = ?, userid = ? where id = ?";
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, question.getQuestion());
			preparedStatement.setString(2, question.getResponse());
			preparedStatement.setInt(3, question.getUser().getId());
			preparedStatement.setInt(4, question.getId());
			return preparedStatement.executeUpdate() < 1 ? Boolean.FALSE : Boolean.TRUE;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public List<Question> questionsResponsed() {
		List<Question> list = new ArrayList<Question>();
		String sql = "select * from question where response is not ?";
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, null);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				list.add(new Question(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3),
						new UserDao(connection).getById(resultSet.getInt(4))));
			}
			return list;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public List<Question> getAll() {
		List<Question> list = new ArrayList<Question>();
		String sql = "select * from question where response is not ?";
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, null);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				list.add(new Question(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3),
						new UserDao(connection).getById(resultSet.getInt(4))));
			}
			sql = "select * from question where response is ?";
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, null);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				list.add(new Question(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3),
						new UserDao(connection).getById(resultSet.getInt(4))));
			}
			return list;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public List<Question> getKeyWord(String key) {
		List<Question> list = new ArrayList<Question>();
		String sql = "select * from question where question REGEXP ? or response REGEXP ? or userid in "
				+ "(select id from user where username REGEXP ?)";
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, key);
			preparedStatement.setString(2, key);
			preparedStatement.setString(3, key);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				list.add(new Question(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3),
						new UserDao(connection).getById(resultSet.getInt(4))));
			}
			
			return list;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
