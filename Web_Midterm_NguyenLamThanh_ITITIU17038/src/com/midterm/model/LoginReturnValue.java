package com.midterm.model;

public class LoginReturnValue {
	private String error;
	private boolean isRegister;

	public LoginReturnValue(String error, boolean isRegister) {
		super();
		this.error = error;
		this.isRegister = isRegister;
	}

	public LoginReturnValue() {
		super();
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public boolean isRegister() {
		return isRegister;
	}

	public void setRegister(boolean isRegister) {
		this.isRegister = isRegister;
	}
}
