package com.midterm.model;

public class Question {
	private int id;
	private String question;
	private String response;
	private User user;
	public Question(int id, String question, String response, User user) {
		super();
		this.id = id;
		this.question = question;
		this.response = response;
		this.user = user;
	}
	public Question() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
