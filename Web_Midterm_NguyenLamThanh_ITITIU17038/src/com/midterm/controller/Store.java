package com.midterm.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.midterm.dao.UserDao;
import com.midterm.model.MyConnect;
import com.midterm.model.User;

/**
 * Servlet implementation class Store
 */
@WebServlet("/store")
public class Store extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Connection connection;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Store() {
		super();
		connection = new MyConnect().getcn();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String fullname = request.getParameter("fullname");
		String email = request.getParameter("email");
		String username = request.getParameter("username");
		String password = request.getParameter("password");

		User user = new User();
		user.setFullname(fullname);
		user.setEmail(email);
		user.setUsername(username);
		user.setPassword(password);
		user.setRole("user");// Default role is user

		// Add to databse
		UserDao userDao = new UserDao(connection);
		userDao.add(user);

		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		String content = "<!DOCTYPE html>\r\n" + "<html>\r\n" + "<head>\r\n"
				+ "<link rel=\"stylesheet\" href='./resources/css/all.css'></link>\r\n"
				+ "<link rel=\"stylesheet\" href='./resources/css/login.css'></link>\r\n"
				+ "<script type=\"text/javascript\" src=\"./resources/js/jquery-3.4.1.min.js\"></script>\r\n"
				+ "<script type=\"text/javascript\" src=\"./resources/js/login.js\"></script>\r\n"
				+ "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n"
				+ "<meta charset=\"UTF-8\">\r\n" + "<style type=\"text/css\">\r\n" + "* {\r\n" + "	margin: 0;\r\n"
				+ "	padding: 0;\r\n" + "	font-family: arial;\r\n" + "	box-sizing: border-box;\r\n" + "}\r\n"
				+ "\r\n" + "body, html {\r\n" + "	width: 100%;\r\n" + "	height: 100%;\r\n" + "}\r\n"
				+ "</style>\r\n" + "<title>Your Information</title>\r\n" + "</head>\r\n" + "<body>\r\n"
				+ "	<div class=\"container\">\r\n"
				+ "		<form class=\"form-container\" action=\"../store\" method=\"post\"\r\n"
				+ "			accept-charset=\"UTF-8\">\r\n" + "			<h1 class=\"title\">Your Information</h1>\r\n"
				+ "			<div class=\"input-container\">\r\n"
				+ "				<i class=\"fas fa-user\"></i> <input type=\"text\"\r\n"
				+ "					placeholder=\"Fullname\" id=\"fullname\" name=\"fullname\" value=\"#fullname\" readonly=\"readonly\">\r\n"
				+ "			</div>\r\n" + "\r\n" + "			<div class=\"input-container\">\r\n"
				+ "				<i class=\"fas fa-envelope\"></i> <input type=\"text\"\r\n"
				+ "					placeholder=\"Email\" id=\"email\" name=\"email\"  value=\"#email\" readonly=\"readonly\">\r\n"
				+ "			</div>\r\n" + "\r\n" + "			<div class=\"input-container\">\r\n"
				+ "				<i class=\"fas fa-user\"></i> <input type=\"text\"\r\n"
				+ "					placeholder=\"User name\" id=\"username\" name=\"username\"  value=\"#username\" readonly=\"readonly\">\r\n"
				+ "			</div>\r\n" + "\r\n" + "			<div class=\"input-container\">\r\n"
				+ "				<i class=\"fas fa-lock\"></i> <input type=\"text\"\r\n"
				+ "					placeholder=\"Password\" id=\"password\" name=\"password\"  value=\"#password\" readonly=\"readonly\">\r\n"
				+ "			</div>\r\n" + "\r\n" + "			<div class=\"input-container\" data-error=\"\">\r\n"
				+ "				<button type=\"button\" id=\"goHomeBtn\">Go home</button>\r\n" + "			</div>\r\n"
				+ "		</form>\r\n" + "	</div>\r\n" + "<input hidden value=\"\">" + "</body>\r\n" + "</html>";

		content = content.replace("#fullname", user.getFullname());
		content = content.replace("#email", user.getEmail());
		content = content.replace("#username", user.getUsername());
		content = content.replace("#password", user.getPassword());
		out.print(content);
	}

}
