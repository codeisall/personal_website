package com.midterm.controller;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.midterm.dao.QuestionDao;
import com.midterm.model.MyConnect;
import com.midterm.model.Question;
import com.midterm.model.User;

/**
 * Servlet implementation class AskServlet
 */
@WebServlet("/askServlet")
public class AskServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Connection connection;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AskServlet() {
		super();
		connection = new MyConnect().getcn();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		String method = request.getParameter("method");
		if (method != null) {
			int questionId = Integer.parseInt(request.getParameter("questionId"));
			QuestionDao questionDao = new QuestionDao(connection);

			Question question = questionDao.getById(questionId);
			System.out.println(question.getId());
			new Gson().toJson(question, response.getWriter());
		} else {
			int type = Integer.parseInt(request.getParameter("type"));
			QuestionDao questionDao = new QuestionDao(connection);

			List<Question> list = new ArrayList<Question>();
			if (type == 1) {
				list = questionDao.questionsResponsed();
			} else if(type == 2) {
				list = questionDao.getAll();
			} else if (type == 3){
				String key = request.getParameter("keyword");
				list = questionDao.getKeyWord(key);
			}
			new Gson().toJson(list, response.getWriter());
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/plain");
		response.setCharacterEncoding("UTF-8");
		QuestionDao questionDao = new QuestionDao(connection);
		String method = request.getParameter("method");
		if (method != null) {
			if (method.equals("update")) {
				int questionId = Integer.parseInt(request.getParameter("questionId"));
				String responseQA = request.getParameter("response");
				if (responseQA.equals("null")) {

				} else {
					Question question = questionDao.getById(questionId);
					question.setResponse(responseQA);
					questionDao.update(question);
				}
			} else if (method.equals("remove")) {
				int questionId = Integer.parseInt(request.getParameter("questionId"));
				Question question = new Question();
				question.setId(questionId);
				questionDao.remove(question);
			}
		} else {
			int userid = Integer.parseInt(request.getParameter("userId"));
			String question = request.getParameter("question");
			User user = new User();
			user.setId(userid);
			questionDao.add(new Question(1, question, null, user));

		}

		response.getWriter().write("Done");
	}
}
