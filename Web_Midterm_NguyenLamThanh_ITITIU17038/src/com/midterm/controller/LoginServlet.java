package com.midterm.controller;

import java.io.IOException;
import java.sql.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.midterm.dao.UserDao;
import com.midterm.model.MyConnect;
import com.midterm.model.User;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/loginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String username = "midterm";
	private String password = "22042020";
	Connection connection;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
		connection = new MyConnect().getcn();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/plain");
		response.setCharacterEncoding("UTF-8");
		String usernameAjax = request.getParameter("username");
		String passwordAjax = request.getParameter("password");
		UserDao userDao = new UserDao(connection);

		// from : after register go to home -> from = register
		// from : in login page -> from = null
		String from = request.getParameter("from");

		if (from != null) {
			User user = userDao.getById(userDao.getLastestUserId());
			HttpSession session = request.getSession(true);
			session.setMaxInactiveInterval(365 * 24 * 60 * 60);
			session.setAttribute("user", user);
			response.sendRedirect("./views/home.jsp");
		} else {
			// Check midterm
			if (usernameAjax.equals(username)) {
				if (passwordAjax.equals(password)) {
					response.getWriter().write("-1-User name already uesed");
				} else {
					response.getWriter().write("Password is incorrect-1-User name already uesed");
				}
			} else {
				User user = userDao.getByUserName(usernameAjax);
				if (user != null) {
					if (user.getPassword().equals(passwordAjax)) {
						response.getWriter().write("-2-User name already uesed");
						HttpSession session = request.getSession(true);
						session.setMaxInactiveInterval(365 * 24 * 60 * 60);
						session.setAttribute("user", userDao.getByUserName(usernameAjax));
					} else {
						response.getWriter().write("Password is incorrect-2-User name already uesed");
					}
				} else {
					response.getWriter().write("Account does not exist-2-");
				}
			}
			// response.getWriter().write("Password is incorrect-2-User name already uesed
			// or not");
			// error message-which account-exsit or not
			// -1: midterm, 2: others-1: exist, 2: not
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		connection = new MyConnect().getcn();
		System.out.println(connection);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write("");
	}

}
