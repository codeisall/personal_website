<%@ include file="library.jsp"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="static.jsp"></jsp:include>
<title></title>
</head>
<body>
	<div class="navbar">
		<span class="logo">NTL</span>
		<ul class="navbar-items">
			<li><a href="./home.jsp" class="active item home">Home</a></li>
			<li><a href="./about.jsp" class="item about">About me</a></li>
			<li><a href="./contact.jsp" class="item contact">Contact me</a></li>
			<li><a href="./ask.jsp" class="item ask">Ask me</a></li>
			<c:if test="${user != null}">
				<li class=""><a class="user"> ${fn:substring(user.username, 0, 2)}
						<span class="user-toggle"> <c:if
								test="${user.role == 'admin'}">
								<span onclick="location.href='./admin_question.jsp'">Manage
									question</span>
							</c:if> <span class="logout-btn"
							onclick="location.href='../logoutServlet'">Logout</span>
					</span>
				</a></li>
			</c:if>
			<c:if test="${user == null}">
				<li><a href="./login.html" class="item">Log in</a></li>
			</c:if>
		</ul>
		<span class="navbar-toggle"><i class="fas fa-bars"></i></span>
	</div>
	<script type="text/javascript">
		var userBtn = document.querySelector("ul.navbar-items li a.user");
		if (userBtn != null) {
			userBtn
					.addEventListener(
							"click",
							function() {
								document
										.querySelector("ul.navbar-items li a.user .user-toggle").classList
										.toggle("appear");
							});
		}
	</script>
</body>
</html>