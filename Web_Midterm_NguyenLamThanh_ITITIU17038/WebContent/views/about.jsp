<%@ include file="library.jsp"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="static.jsp"></jsp:include>
<link rel="stylesheet" href='../resources/css/about.css'></link>
<title>About Me</title>
</head>
<body>
	<div class="container">
		<jsp:include page="header.jsp"></jsp:include>
		<div class="content">
			<div class="welcome">
				<h3>Hello</h3>
				<h1>I'm Nguyen Lam Thanh (Michael)</h1>
			</div>
			<div class="private-info">
				<h1>Information</h1>
				<div class="infors-container">
					<div>
						<p>Full name</p>
						<p>Nguyen Lam Thanh</p>
					</div>
					<div>
						<p>Birth of date</p>
						<p>14 - 05 - 1999</p>
					</div>
					<div>
						<p>Phone</p>
						<p>09xxxxxxxx</p>
					</div>
					<div>
						<p>Email</p>
						<p>nlthanhititiu17038@gmail.com</p>
					</div>
					<div>
						<p>School</p>
						<p>
							<a href="https://hcmiu.edu.vn/" target="_blank">International
								University - VNU</a>
						</p>
					</div>
					<div>
						<p>Facebook</p>
						<p>
							<a href="https://www.facebook.com/lamthanhit" target="_blank">Nguyen
								Lam Thanh</a>
						</p>
					</div>
					<div>
						<p>Github</p>
						<p>
							<a href="https://github.com/webapplicationkhthth" target="_blank">Nguyen
								Lam Thanh</a>
						</p>
					</div>
					<div>
						<p>Bitbucket</p>
						<p>
							<a href="https://bitbucket.org/codeisall/" target="_blank">Nguyen
								Lam Thanh</a>
						</p>
					</div>
				</div>
			</div>

			<div class="skills">
				<h1>Skills</h1>
				<div class="skills-container">
					<div class="skill">
						<p>HTML/CSS</p>
						<p></p>
					</div>
					<div class="skill">
						<p>JavaScript</p>
						<p></p>
					</div>
					<div class="skill">
						<p>JQuery</p>
						<p></p>
					</div>
					<div class="skill">
						<p>Responsive Design</p>
						<p></p>
					</div>
					<div class="skill">
						<p>Spring MVC</p>
						<p></p>
					</div>
				</div>
			</div>
		</div>
		<jsp:include page="footer.jsp"></jsp:include>
	</div>
</body>
</html>