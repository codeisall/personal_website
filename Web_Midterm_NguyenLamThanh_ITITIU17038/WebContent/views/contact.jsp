<%@ include file="library.jsp"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="static.jsp"></jsp:include>
<link rel="stylesheet" href='../resources/css/contact.css'></link>
<title>Contact</title>
</head>
<body>
	<div class="container">
		<jsp:include page="header.jsp"></jsp:include>
		<div class="content">
			<h1>Contact me</h1>
			<form action="" method="post">
				<h1>Google Form</h1>
				<div class="form-input">
					<div class="emalil_container">
						<label for="email">Email</label> <input id="email"
							placeholder="Email">
					</div>

					<div class="feedback_container">
						<label for="feedback">Your feedback</label>
						<textarea id="feedback" rows="8" cols="50"
							placeholder="Your feedback..."
							class="quantumWizTextinputPapertextareaInput exportTextarea"></textarea>
					</div>
					<button type="button">Send</button>
				</div>
			</form>
		</div>
		<div class="feedback_success">
			<span class="feedback_success_close"><i class="fas fa-times"></i></span>
			Thanks for your feedback!!!
		</div>
		<div class="blur"></div>
		
		<jsp:include page="footer.jsp"></jsp:include>
	</div>
</body>
</html>