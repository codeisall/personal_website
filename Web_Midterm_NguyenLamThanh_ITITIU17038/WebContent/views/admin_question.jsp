<%@ include file="library.jsp"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="static.jsp"></jsp:include>
<link rel="stylesheet" href='../resources/css/ask.css'></link>
<script type="text/javascript" src="../resources/js/ask.js"></script>
<title>Manage Question(s)</title>
</head>
<body data-page="admin">
	<c:if test="${user != null}">
		<c:if test="${user.role == 'admin'}">
			<div class="container"
				style="background-color: #dcdde1; height: 100vh; position: relative;">
				<h1 style="text-align: center; padding: 10px 0 20px 0;">Manage
					User's Question(s)</h1>
				<div class="content" style="padding: 0;">
					<h1>Q and A</h1>
					<div class="qa-container" style="height: 700px;"></div>
				</div>
				<div class="buttons">
					<a href="./home.jsp">Home</a> <a href="../logoutServlet">Log
						out</a>
				</div>
				<div class="answer-box">
					<div>
						<p>Question</p>
						<p class="answer-box-question"></p>
					</div>
					<div>
						<p>Response</p>
						<textarea rows="10" cols="10" placeholder="Answer..."
							class="answer-box-answer"></textarea>
					</div>
					<div>
						<button type="button">Update</button>
					</div>
					<i class="fas fa-times"></i>
				</div>
				<div class="blur"></div>
			</div>
		</c:if>
		<c:if test="${user.role != 'admin'}">
			<h1 style="text-align: center; color: red; font-size: 50px;">
				You are not the permission to access this page! <br> You will
				be redirect to home page after 5s!!!
			</h1>
			<script type="text/javascript">
				setTimeout(function() {
					location.href = "./home.jsp";
				}, 5000)
			</script>
		</c:if>
	</c:if>
	<c:if test="${user == null}">
		<h1 style="text-align: center; color: red; font-size: 50px;">
			You MUST login first! <br> You will be redirect to login page
			after 5s!!!
		</h1>
		<script type="text/javascript">
			setTimeout(function() {
				location.href = "./login.html";
			}, 5000)
		</script>
	</c:if>
</body>
</html>