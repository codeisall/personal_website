<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="UTF-8">
<title></title>
</head>
<body>
	<footer class="footer">
		<div class="footer_top">
			<div class="footer_top_left">
				<p class="postion">
					<i class="fas fa-map-marker-alt"></i> <span>International
						University - VNU</span>
				</p>
				<p class="phone_number">
					<i class="fas fa-phone"></i> <span>+8499999999</span>
				</p>
				<p class="email">
					<i class="fas fa-envelope"></i> <span>abc@hcmiu.com.vn</span>
				</p>
			</div>
			<div class="footer_top_right">
				<div class="socials">
					<i class="fab fa-facebook-f"></i> <i class="fab fa-youtube"></i>
				</div>
			</div>
		</div>
		<div class="footer_copyright">
			<i class="far fa-copyright"></i><span>Nguyen Lam Thanh - CSE.
				All Rights Reserved.</span>
		</div>
	</footer>
</body>
</html>