<%@ include file="library.jsp"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="static.jsp"></jsp:include>
<link rel="stylesheet" href='../resources/css/ask.css'></link>
<script type="text/javascript" src="../resources/js/ask.js"></script>
<title>Ask Me</title>
</head>
<body data-page="ask">
	<div class="container">
		<jsp:include page="header.jsp"></jsp:include>
		<div class="content">
			<h1>Q and A<i class="fas fa-search"></i></h1>
			<div class="qa-container"></div>
			<div class="ask-me">
				<h1>Ask me</h1>
				<div class="question-container">
					<h2>Your question</h2>
					<textarea rows="10" cols="10" placeholder="Question...."></textarea>
				</div>
				<div class="button-container">
					<button type="button" value="${user!=null?user.id:'null'}">Send</button>
				</div>
			</div>
		</div>
		<div class="search-qa">
		<h1>Your result <i class="fas fa-times"></i></h1>
		<input type="text" placeholder="Key words, username...">
		<div class="qa-container"></div>
		</div>
		<div class="blur"></div>
		<jsp:include page="footer.jsp"></jsp:include>
	</div>
</body>
</html>