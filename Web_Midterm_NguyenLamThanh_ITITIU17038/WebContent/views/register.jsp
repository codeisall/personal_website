<%@ include file="library.jsp"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="static.jsp"></jsp:include>
<link rel="stylesheet" href='../resources/css/login.css'></link>
<script type="text/javascript" src="../resources/js/login.js"></script>
<meta charset="UTF-8">
<title>Member Registration</title>
</head>
<body>
	<div class="container">
		<form class="form-container" action="../store" method="post" accept-charset="UTF-8">
			<h1 class="title">Member Registration</h1>
			<div class="input-container">
				<i class="fas fa-user"></i> <input type="text"
					placeholder="Fullname" id="fullname" name="fullname">
			</div>
			
			<div class="input-container">
				<i class="fas fa-envelope"></i> <input type="text" placeholder="Email"
					id="email" name="email">
			</div>
			
			<div class="input-container">
				<i class="fas fa-user"></i> <input type="text"
					placeholder="User name" id="username" name="username">
			</div>

			<div class="input-container">
				<i class="fas fa-lock"></i> <input type="password"
					placeholder="Password" id="password" name="password">
			</div>
			
			<div class="input-container">
				<i class="fas fa-check-double"></i> <input type="password"
					placeholder="Confirm password" id="confirm">
			</div>
			
			<div class="input-container" data-error="">
				<button type="submit" id="registerBtn"> Register </button>
			</div>
		</form>
	</div>
</body>
</html>