<%@ include file="library.jsp"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="static.jsp"></jsp:include>
<link rel="stylesheet" href='../resources/css/home.css'></link>
<script type="text/javascript" src="../resources/js/home.js"></script>
<title>Home</title>
</head>
<body>
	<div class="container">
		<jsp:include page="header.jsp"></jsp:include>
		<div class="banner">
			<i class="fas fa-chevron-right"></i> <i class="fas fa-chevron-left"></i>
			<div class="numbers">
				<span class="active">1</span> <span>2</span> <span>3</span>
			</div>
			<div class="img active"
				style="background-image: url(../resources/img/fr.jpg); background-size: cover;"
				data-order="1"></div>
			<div class="img"
				style="background-image: url(../resources/img/spring.jpg); background-size: cover;"
				data-order="2"></div>
			<div class="img"
				style="background-image: url(../resources/img/full.jpg); background-size: cover;"
				data-order="3"></div>
		</div>
		<div class="content">
			<div class="hobbies-container">
				<h1>Hobbies</h1>
				<div class="hobbies">
					<div class="hobby">
						<i class="fas fa-laptop-code"></i>
						<h3>Code</h3>
						<p>The activity I like the most is coding. I really like to
							work with code, everyday i try my best to solve some problems
							from my subjects in school. When i work with code, i feel very
							excited. The feeling when i find out the solution is amazing.</p>
					</div>
					<div class="hobby">
						<i class="fas fa-comments"></i>
						<h3>Talk</h3>
						<p>Talking to my colleagues helps me so much. We can exchange
							knowledge. The topic that i want to talk with my friends the most
							is the experience of our work.</p>
					</div>
					<div class="hobby">
						<i class="fab fa-leanpub"></i>
						<h3>Learn</h3>
						<p>This hobby is pretty bored. But, i also make this activity
							like a hobby. I usually search for programming courses on the
							internet for learning. i aslo try to improve my English through
							online documents.</p>
					</div>
				</div>
			</div>
			<div class="projects-container">
				<h1>Projects Joined</h1>
				<div class="projects">
					<div class="front-end type">
						<h3>
							Front End <i class="fas fa-chevron-down"
								data-name="fron-end-projects"></i>
						</h3>
						<p>Following projects are individual works</p>
						<div class="fron-end-projects">
							<div class="project">
								<div>
									<i class="fas fa-map-marker"></i>
									<p>Home</p>
								</div>
								<div>
									<i class="fas fa-file-signature"></i>
									<p>BeTheme Home Page</p>
								</div>
								<div>
									<i class="fas fa-paperclip"></i>
									<p class="link"
										onclick="window.open('https://drive.google.com/drive/u/1/folders/1fIDo4Y2WLuhONTKh5xDhWfjhsLkDiUvy', '_blank');">More
										detail</p>
								</div>
							</div>
							<div class="project">
								<div>
									<i class="fas fa-map-marker"></i>
									<p>IU-VNU</p>
								</div>
								<div>
									<i class="fas fa-file-signature"></i>
									<p>2048 Game (Customized)</p>
								</div>
								<div>
									<i class="fas fa-paperclip"></i>
									<p class="link"
										onclick="window.open('https://bitbucket.org/codeisall/2048_project/src/master/', '_blank');">More
										detail</p>
								</div>
							</div>
						</div>
					</div>
					<div class="back-end type">
						<h3>
							Back End <i class="fas fa-chevron-down"
								data-name="back-end-projects"></i>
						</h3>
						<p>Following projects are the ones that i also worked in group</p>
						<div class="back-end-projects">
							<div class="project">
								<div>
									<i class="fas fa-map-marker"></i>
									<p>IU-VNU</p>
								</div>
								<div>
									<i class="fas fa-file-signature"></i>
									<p>Library Management System</p>
								</div>
								<div>
									<i class="fas fa-paperclip"></i>
									<p class="link"
										onclick="window.open('https://bitbucket.org/pdm2019/pdm-project/src/master/', '_blank');">More
										detail</p>
								</div>
							</div>
						</div>
					</div>
					<div class="full-stack type">
						<h3>
							Full Stack <i class="fas fa-chevron-down"
								data-name="full-stack-projects"></i>
						</h3>
						<p>Following projects are the ones that i worked in group</p>
						<div class="full-stack-projects">
							<div class="project">
								<div>
									<i class="fas fa-map-marker"></i>
									<p>Green Academy</p>
								</div>
								<div>
									<i class="fas fa-file-signature"></i>
									<p>TATATLO - E-commerce</p>
								</div>
								<div>
									<i class="fas fa-paperclip"></i>
									<p class="link"
										onclick="window.open('https://bitbucket.org/jdevd006/final_project_v2/src/master/', '_blank');">More
										detail</p>
								</div>
							</div>
							<div class="project">
								<div>
									<i class="fas fa-map-marker"></i>
									<p>IU-VNU</p>
								</div>
								<div>
									<i class="fas fa-file-signature"></i>
									<p>Library Management System</p>
								</div>
								<div>
									<i class="fas fa-paperclip"></i>
									<p class="link"
										onclick="window.open('https://bitbucket.org/sekhthth/library-management-system/src/master/', '_blank');">More
										detail</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<jsp:include page="footer.jsp"></jsp:include>
	</div>
</body>
</html>