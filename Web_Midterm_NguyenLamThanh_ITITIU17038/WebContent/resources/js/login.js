window
		.addEventListener(
				"DOMContentLoaded",
				function() {
					var inputs = document
							.querySelectorAll(".input-container input");
					var loginBtn = document
							.querySelector(".input-container input#loginBtn");
					var registerBtn = document
							.querySelector(".input-container button#registerBtn");
					var goHomeBtn = document
							.querySelector(".input-container button#goHomeBtn");
					var spacePattern = /[ \t]+/g;
					var emailPattern = /[a-zA-Z0-9]+@gmail.com/g;
					var isUserNameAndPasswordValid = false;

					for (var i = 0; i < inputs.length - 1; i++) {
						inputs[i].addEventListener("focus", function() {
							for (var j = 0; j < inputs.length - 1; j++) {
								inputs[j].parentElement.classList
										.remove("focused");
							}
							this.parentElement.classList.add("focused");
						});
					}

					window.addEventListener("click", function(e) {
						var id = e.target.id;
						if (id != "username" && id != "password"
								&& id != "fullname" && id != "email"
								&& id != "confirm") {
							for (var j = 0; j < inputs.length - 1; j++) {
								inputs[j].parentElement.classList
										.remove("focused");
							}
						}
					});

					if (loginBtn != null) { // login page
						loginBtn.addEventListener("click", function() {
							var usernameInput = document
									.querySelector("#username").value;
							var passwordInput = document
									.querySelector("#password").value;

							checkUserNameAndPassword(usernameInput,
									passwordInput, 1);
						});
					} else if (registerBtn != null) { // register page
						registerBtn
								.addEventListener(
										"click",
										function(event) {
											event.preventDefault();
											var usernameInput = document
													.querySelector("#username").value;
											var passwordInput = document
													.querySelector("#password").value;
											var fullnameInput = document
													.querySelector("#fullname").value;
											var emailInput = document
													.querySelector("#email").value;
											var confirmInput = document
													.querySelector("#confirm").value;

											if (!isEmpty(fullnameInput)) {
												// if (!hasSpace(fullnameInput))
												// {
												if (!isEmpty(emailInput)) {
													if (!hasSpace(emailInput)) {
														if (isValidEmail(emailInput)) {
															checkUserNameAndPassword(
																	usernameInput,
																	passwordInput,
																	2);
															if (isUserNameAndPasswordValid) {
																isUserNameAndPasswordValid = false;
																if (!isEmpty(confirmInput)) {
																	if (!hasSpace(confirmInput)) {
																		if (confirmInput == passwordInput) {
																			document
																					.querySelector(
																							".form-container")
																					.submit();
																		} else {
																			displayError("Confirm password does not match password");
																		}
																	} else {
																		displayError("Confirm password cannot contain any space letter!");
																	}
																} else {
																	displayError("Confirm password cannot be empty!");
																}
															}
														} else {
															displayError("Email is not valid!");
														}
													} else {
														displayError("Email cannot contain any space letter!");
													}
												} else {
													displayError("Email cannot be empty!");
												}
												// } else {
												// displayError("Full name
												// cannot contain any space
												// letter!");
												// }
											} else {
												displayError("Full name cannot be empty!");
											}

										});
					} else { // your information go to home
						goHomeBtn.addEventListener("click", function() {
							console.log("Go to home | your information");
							location.href = "./loginServlet?from=register";
						});
					}

					// type = 1 || 2
					// 1 : login page
					// 2 : register page
					function checkUserNameAndPassword(usernameInput,
							passwordInput, type) {
						if (!isEmpty(usernameInput)) {
							if (!hasSpace(usernameInput)) {
								if (!isEmpty(passwordInput)) {
									if (!hasSpace(passwordInput)) {
										$
												.ajax({
													url : "../loginServlet",
													type : "get",
													data : {
														username : usernameInput,
														password : passwordInput
													},
													contentType : "text/plain; charset=utf-8",
													success : function(data) {
														console.log(data);
														if (data.split('-')[0] == "") {
															if (data.split('-')[1] == "1") {
																if (type == 1)
																	location.href = "./register.jsp";
																else
																	displayError(data
																			.split('-')[2]);
															} else {
																if (type == 1) {
																	console
																			.log("Go to home");
																	location.href = "./home.jsp";
																} else
																	displayError(data
																			.split('-')[2]);
															}
														} else {
															if (type == 1) {
																displayError(data
																		.split('-')[0]);
															} else {
																if (data
																		.split('-')[2] == "") {
																	isUserNameAndPasswordValid = true;
																} else {
																	displayError(data
																			.split('-')[2]);
																}
															}
														}
													},
													async : false,
													error : function(e) {
														console.log(e);
													}
												});
									} else {
										displayError("Password cannot contain any space letter!");
									}
								} else {
									displayError("Password cannot be empty!");
								}
							} else {
								displayError("User name cannot contain any space letter!");
							}
						} else {
							displayError("User name cannot be empty!");
						}
					}

					function displayError(msg) {
						var errorContainer = document
								.querySelector(".input-container:last-child");
						errorContainer.classList.remove("error");
						errorContainer.addEventListener("webkitTransitionEnd",
								function() {
									errorContainer.dataset.error = msg;
									this.classList.add("error");
								});
					}
					function isEmpty(str) {
						return str.trim().length > 0 ? false : true;
					}
					function hasSpace(str) {
						return str.match(spacePattern) != null ? true : false;
					}
					function isValidEmail(str) {
						return str.match(emailPattern) != null ? true : false;
					}
				});