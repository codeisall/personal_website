window.addEventListener("DOMContentLoaded", function() {
	var bannerImgs = document.querySelectorAll(".banner div.img");
	var bannerNumbers = document.querySelectorAll(".banner div.numbers span");
	var projectExpandBtns = document.querySelectorAll(".type h3 i");
	var preBannerBtn = document.querySelector(".banner i.fa-chevron-left");
	var nextBannerBtn = document.querySelector(".banner i.fa-chevron-right");
	var autoBannerInterval;
	var preClick = false;
	preBannerBtn.addEventListener("click", function() {
//		if (!preClick) {
//			clearInterval(autoBannerInterval);
			preClick = true;
			var numberOrder = -1;
			var activeImg = document.querySelector(".banner .img.active");
			var nextImg = activeImg.nextElementSibling;

			bannerNumbers[parseInt(activeImg.dataset.order) - 1].classList
					.remove("active");

			if (nextImg == null) {
				nextImg = bannerImgs[0];
				numberOrder = 0;
			} else {
				numberOrder = parseInt(nextImg.dataset.order) - 1;
			}

			bannerNumbers[numberOrder].classList.add("active");

			activeImg.classList.add("move-left-active");
			nextImg.classList.add("move-left-next");
			activeImg.addEventListener("webkitAnimationEnd", function() {
				this.classList.remove("active");
				this.classList.remove("move-left-active");
			});
			nextImg.addEventListener("webkitAnimationEnd", function() {
				this.classList.remove("move-left-next");
				this.classList.add("active");
				preClick = false;
			});
//		}
	});

});