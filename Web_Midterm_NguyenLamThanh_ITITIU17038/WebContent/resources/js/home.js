window.addEventListener("DOMContentLoaded", function() {
	var bannerImgs = document.querySelectorAll(".banner div.img");
	var bannerNumbers = document.querySelectorAll(".banner div.numbers span");
	var projectExpandBtns = document.querySelectorAll(".type h3 i");
	var preBannerBtn = document.querySelector(".banner i.fa-chevron-left");
	var nextBannerBtn = document.querySelector(".banner i.fa-chevron-right");
	var autoBannerInterval;
	preBannerBtn.addEventListener("click", function() {
			clearInterval(autoBannerInterval);
			var numberOrder = -1;
			var activeImg = document.querySelector(".banner .img.active");
			var nextImg = activeImg.nextElementSibling;

			bannerNumbers[parseInt(activeImg.dataset.order) - 1].classList
					.remove("active");

			if (nextImg == null) {
				nextImg = bannerImgs[0];
				numberOrder = 0;
			} else {
				numberOrder = parseInt(nextImg.dataset.order) - 1;
			}

			bannerNumbers[numberOrder].classList.add("active");

			activeImg.classList.add("move-left-active");
			nextImg.classList.add("move-left-next");
			activeImg.addEventListener("webkitAnimationEnd", function() {
				this.classList.remove("active");
				this.classList.remove("move-left-active");
			});
			nextImg.addEventListener("webkitAnimationEnd", function() {
				this.classList.remove("move-left-next");
				this.classList.add("active");
			});
//		}
	});

	nextBannerBtn.addEventListener("click", function() {
		clearInterval(autoBannerInterval);
		nextBannerForAuto();
	});

	function nextBannerForAuto() {
		var activeImg = document.querySelector(".banner .img.active");
		var preImg = activeImg.previousElementSibling;

		bannerNumbers[parseInt(activeImg.dataset.order) - 1].classList
				.remove("active");

		if (preImg.classList.contains("numbers")) {
			preImg = bannerImgs[bannerImgs.length - 1];
			numberOrder = bannerImgs.length - 1;
		} else {
			numberOrder = parseInt(preImg.dataset.order) - 1;
		}

		bannerNumbers[numberOrder].classList.add("active");

		activeImg.classList.add("move-right-active");
		preImg.classList.add("move-right-pre");
		activeImg.addEventListener("webkitAnimationEnd", function() {
			this.classList.remove("active");
			this.classList.remove("move-right-active");
		});
		preImg.addEventListener("webkitAnimationEnd", function() {
			this.classList.remove("move-right-pre");
			this.classList.add("active");
		});
	}

	autoBannerInterval = setInterval(function() {
		nextBannerForAuto();
	}, 3000);
	
	for(var i = 0; i < projectExpandBtns.length; i++) {
		projectExpandBtns[i].addEventListener("click", function () {
			document.querySelector("." + this.dataset.name).classList.toggle("project-expand");
		});
	}
});