window.addEventListener("DOMContentLoaded", function() {
	var navbarItems = document.querySelectorAll("ul.navbar-items li a");
	var navbar = document.querySelector(".navbar");
	
	for (var i = 0; i < navbarItems.length; i++) {
		navbarItems[i].addEventListener("mouseover", function() {
			for (var j = 0; j < navbarItems.length-1; j++) {
				navbarItems[j].parentElement.classList.remove("mouse-on");	
			}
			this.parentElement.classList.add("mouse-on");
		});
	}
	
	window.addEventListener("mouseover", function(e) {
		var id = e.target.classList;
		if (!id.contains("item")) {
			for (var j = 0; j < navbarItems.length; j++) {
				navbarItems[j].parentElement.classList.remove("mouse-on");	
			}
		}
	});

	
	
	var isOver = false;
	// Nav
	window.onscroll = function() {
		var vertical = window.pageYOffset;
		if (vertical > 60) {
			if (!isOver) {
				navbar.classList.add("nav_bar_fixed");
			}
			isOver = true;
		} else {
			if (isOver) {
				navbar.classList.remove("nav_bar_fixed");
			}
			isOver = false;
		}
	};
	
	
	if(document.querySelector(".form-input button") != null){
		document.querySelector(".form-input button").addEventListener("click", function() {
			$.ajax({
				url : "https://docs.google.com/forms/u/0/d/e/1FAIpQLScKaAmhFUf_o_jMXjHcjU-wAoJHzr-3i0didTlbGQZStUraUw/formResponse",
				type : "POST",
				data : {
					"entry.1671984691" : document.querySelector(".feedback_container textarea").value
				},
				dataType: "jsonp",
				// form-inputType : "application/json; charset=utf-8",
				jsonpCallBack : function(){alert(2)},
				success : function(data) {
					alert(1);
				},
				error: function(jqXHR, textStatus, errorThrown) {
			        // When AJAX call has failed
			        console.log('AJAX call failed.');
			        console.log(textStatus + ': ' + errorThrown);
			    },
			});
	
			document.querySelector(".feedback_success").classList.add("feedback_success_appear");
			document.querySelector(".blur").classList.add("blur_appear");
		});
	}
	
	if(document.querySelector(".feedback_success_close") != null){
		document.querySelector(".feedback_success_close").addEventListener("click", function() {
			document.querySelector(".feedback_success").classList.remove("feedback_success_appear");
			document.querySelector(".blur").classList.remove("blur_appear");
		});
	}
	
	if(document.querySelector(".blur") != null){
		document.querySelector(".blur").addEventListener("click", function() {
			document.querySelector(".blur").classList.remove("blur_appear");
			if(document.querySelector(".feedback_success") != null) {
				if(document.querySelector(".feedback_success").classList.contains("feedback_success_appear")) {
					document.querySelector(".feedback_success").classList.remove("feedback_success_appear");
				} else {
					document.querySelector(".menu").classList.remove("slide_nav");
				}
			}
			if(document.querySelector(".search-qa") != null) {
				document.querySelector(".search-qa").classList.remove("appear");
			}
		});
	}
	
});