window.addEventListener("DOMContentLoaded", function() {
	var page = document.querySelector("body").dataset.page=="ask"?1:2;
	
	var content = "<i class=\"fas fa-chevron-right\"></i> <i class=\"fas fa-chevron-left\"></i>\r\n" + 
	"				<div class=\"numbers\">\r\n" + 
	"					#numbers\r\n"+"</div>"+"#content";
	var qasContainer = "<div class=\"qas #active\" data-order=\"#order\">\r\n" + 
	"					#listqa\r\n" + 
	"				</div>";
	var qa = "<div class=\"qa\" id=\"qa#id\">\r\n" + 
	"						<div>\r\n" + 
	"							<p>From</p>\r\n" + 
	"							<p>#from</p>\r\n" + 
	"						</div>\r\n" + 
	"						<div>\r\n" + 
	"							<p>Question</p>\r\n" + 
	"							<p>#question</p>\r\n" + 
	"						</div>\r\n" + 
	"						<div>\r\n" + 
	"							<p>Response</p>\r\n" + 
	"							<p>#response</p>\r\n" + 
	"						</div>\r\n" + 
	"                       #button"+
	"					</div>";
	var span = "<span class='#active'>#number</span>";
	
	var button = "<div>\r\n" + 
	"						<button type=\"button\" value=\"#questionid\">Remove</button>\r\n" + 
	"						<button type=\"button\" value=\"#questionid\">Answer</button>\r\n" + 
	"					</div>";
	
	if(page == 2) {
		qa = qa.replace("#button", button);
	} else {
		qa = qa.replace("#button", "");
	}	
	
	getListQuestion(1, "");
	
	function getListQuestion(type, keyword){
		var tmp = 0;
		var urlTmp = "";
		if(type == 1) { //1 : normal
			tmp = page;
			urlTmp = "../askServlet?type="+tmp;
		} else { //2 : search
			tmp = 3;
			urlTmp = "../askServlet?type="+tmp;
			urlTmp += "&keyword=" + keyword;
		}
		
		$.ajax({
			url : urlTmp,
			type : "get",
			data : {
			},
			contentType : "application/json; charset=utf-8",
			success : function(data) {
				console.log(data)
				var contentTmp = content;
				var numberOfPage = 0;
				var spanTemp = "";
				var spans = "";
				var qaTemp = "";
				var qas = "";
				var qasContainerTmp = "";
				var qasContainers = "";
				var countQuestion = 0;
				if(data.length%5 == 0) {
					numberOfPage = Math.floor(data.length/5);
				} else {
					numberOfPage = Math.floor(data.length/5) + 1;
				}
				
				for(var i = 0; i < numberOfPage; i++) {
					spanTemp = span;
					qasContainerTmp = qasContainer;
					if(i == 0) {
						spanTemp = spanTemp.replace("#active", "active");
						qasContainerTmp = qasContainerTmp.replace("#active", "active");
					}else {
						spanTemp = spanTemp.replace("#active", "");
						qasContainerTmp = qasContainerTmp.replace("#active", "");
					}
					spanTemp = spanTemp.replace("#number", i+1);
					spans += spanTemp;
					var count = 1;
					qas = "";
					while(count <= 5) {
						if(countQuestion >= data.length) break;
						qaTemp = qa;
						qaTemp = qaTemp.replace("#from", data[countQuestion].user.fullname);
						qaTemp = qaTemp.replace("#question", data[countQuestion].question);
						qaTemp = qaTemp.replace("#response", data[countQuestion].response);
						if(page == 2) {
							qaTemp = qaTemp.replace("#id", data[countQuestion].id);
							qaTemp = qaTemp.replace("#questionid", data[countQuestion].id);
							qaTemp = qaTemp.replace("#questionid", data[countQuestion].id);
						}
						count++;
						countQuestion++;
						qas += qaTemp;
					
					}
					qasContainerTmp = qasContainerTmp.replace("#listqa", qas);
					qasContainerTmp = qasContainerTmp.replace("#order", i+1);
					
					qasContainers += qasContainerTmp;
				}
				contentTmp = contentTmp.replace("#numbers", spans);
				contentTmp = contentTmp.replace("#content", qasContainers);
				
				if(type == 1)
					document.querySelector(".qa-container").innerHTML = contentTmp;
				else if(type == 2)
					document.querySelector(".search-qa .qa-container").innerHTML = contentTmp;
			},
			async : false,
			error : function(e) {
				console.log(e);
			}
		});
	}
	
	var bannerImgs = document.querySelectorAll(".qa-container .qas");
	var bannerNumbers = document.querySelectorAll(".qa-container div.numbers span");
	var preBannerBtn = document.querySelector(".qa-container i.fa-chevron-left");
	var nextBannerBtn = document.querySelector(".qa-container i.fa-chevron-right");
	var sendQuestionBtn = document.querySelector(".button-container button");
	var autoBannerInterval;
	
	if(preBannerBtn != null){
		preBannerBtn.addEventListener("click", function() {
			clearInterval(autoBannerInterval);
			var numberOrder = -1;
			var activeImg = document.querySelector(".qa-container .qas.active");
			var nextImg = activeImg.nextElementSibling;
		
			bannerNumbers[parseInt(activeImg.dataset.order) - 1].classList
			.remove("active");
		
			if (nextImg == null) {
				nextImg = bannerImgs[0];
				numberOrder = 0;
			} else {
				numberOrder = parseInt(nextImg.dataset.order) - 1;
			}
		
			bannerNumbers[numberOrder].classList.add("active");
		
			activeImg.classList.add("move-left-active");
			nextImg.classList.add("move-left-next");
			activeImg.addEventListener("webkitAnimationEnd", function() {
				this.classList.remove("active");
				this.classList.remove("move-left-active");
			});
			
			nextImg.addEventListener("webkitAnimationEnd", function() {
				console.log(2)
				this.classList.remove("move-left-next");
				this.classList.add("active");
				preClick = false;
			});
		});
	}

	
	if(nextBannerBtn != null){
		nextBannerBtn.addEventListener("click", function() {
			nextBannerForAuto();
		});
	}
	
	function nextBannerForAuto() {
	nextClick = true;
	var activeImg = document.querySelector(".qa-container .qas.active");
	var preImg = activeImg.previousElementSibling;
	
	bannerNumbers[parseInt(activeImg.dataset.order) - 1].classList
	.remove("active");
	
	if (preImg.classList.contains("numbers")) {
		preImg = bannerImgs[bannerImgs.length - 1];
		numberOrder = bannerImgs.length - 1;
	} else {
		numberOrder = parseInt(preImg.dataset.order) - 1;
	}
	
	bannerNumbers[numberOrder].classList.add("active");
	
	activeImg.classList.add("move-right-active");
	preImg.classList.add("move-right-pre");
	activeImg.addEventListener("webkitAnimationEnd", function() {
		this.classList.remove("active");
		this.classList.remove("move-right-active");
	});
	preImg.addEventListener("webkitAnimationEnd", function() {
		this.classList.remove("move-right-pre");
		this.classList.add("active");
		nextClick = false;
	});
	}
	
	if(sendQuestionBtn != null) {
		sendQuestionBtn.addEventListener("click", function() {
			var userid = this.value;
			var questionTextarea = document.querySelector(".question-container textarea").value;
			if(userid == "null") {
				var confirmBox = confirm("You must log in first!!!\n Do you to come to login page?");
				if(confirmBox) {
					location.href = "./login.html";
				}
			} else {
				$.ajax({
					url : "../askServlet?userId="+userid+"&question=" + questionTextarea,
					type : "post",
					data : {
					},
					contentType : "text/plain; charset=utf-8",
					success : function(data) {
						if(data == "Done"){
							alert("Thanks for your question!!!\n I will reply to you as soon as possible.");
							document.querySelector(".question-container textarea").value = "";
							location.reload();
						}
					},
					async : false,
					error : function(e) {
						console.log(e);
					}
				});
			}
		});
	}
	
	if(page == 2) {
		var removeBtns = document.querySelectorAll(".qa button:first-child");
		var answerBtns = document.querySelectorAll(".qa button:last-child");
		var updateBtn = document.querySelector(".answer-box button");
		
		for(var i = 0; i < removeBtns.length; i++) {
			removeBtns[i].addEventListener("click", function() {
				var confirmRemove = confirm("Do you really remove this record?");
				if(confirmRemove) {
					$.ajax({
						url : "../askServlet?method=remove&questionId="+this.value,
						type : "post",
						data : {
						},
						contentType : "text/plain; charset=utf-8",
						success : function(data) {
							alert("Successfully!");
							location.reload();
						},
						async : false,
						error : function(e) {
							console.log(e);
						}
					});
				}
			});
		}
		
		for(var i = 0; i < answerBtns.length; i++) {
			answerBtns[i].addEventListener("click", function() {
				updateBtn.value = this.value;
				document.querySelector(".blur").classList.add("blur_appear");
				document.querySelector(".answer-box").classList.add("answer-box-appear");
				$.ajax({
					url : "../askServlet?method=get&questionId="+this.value,
					type : "get",
					data : {
					},
					contentType : "application/json; charset=utf-8",
					success : function(data) {
						document.querySelector(".answer-box-question").textContent = data.question;
					},
					async : false,
					error : function(e) {
						console.log(e);
					}
				});
			});
		}
		
		updateBtn.addEventListener("click", function() {
			var response = document.querySelector(".answer-box textarea").value.trim();
			if(response.length <= 0 ) response = "null";
			$.ajax({
				url : "../askServlet?method=update&questionId="+this.value+"&response=" + response,
				type : "post",
				data : {
				},
				contentType : "text/plain; charset=utf-8",
				success : function(data) {
					alert("Successfully!");
					location.reload();
				},
				async : false,
				error : function(e) {
					console.log(e);
				}
			});
		});
		
		document.querySelector(".answer-box i.fas.fa-times").addEventListener("click", function() {
			document.querySelector(".answer-box").classList.remove("answer-box-appear");
			document.querySelector(".blur").classList.remove("blur_appear");
		});
		
		
		document.querySelector(".blur").addEventListener("click", function() {
			document.querySelector(".answer-box").classList.remove("answer-box-appear");
			document.querySelector(".blur").classList.remove("blur_appear");
		});
	}
	

	var searchInput = document.querySelector(".search-qa input");
	if (searchInput!= null) {
		searchInput.addEventListener("keyup", function() {
			if(this.value.trim().length <= 0) {
				document.querySelector(".search-qa .qa-container").innerHTML = "<h2 style='text-align:center;color:red;'>Empty!!!</h2>"
			} else {
				getListQuestion(2, this.value);
			}
		});
	}
	
	var searchBtn = document.querySelector(".content h1 i");
	var closeSeachBoxBtn = document.querySelector(".search-qa h1 i");
	
	if(searchBtn != null) {
		searchBtn.addEventListener("click", function() {
			document.querySelector(".search-qa .qa-container").innerHTML = "<h2 style='text-align:center;color:red;'>Empty!!!</h2>";
			document.querySelector(".search-qa").classList.add("appear");
			document.querySelector(".blur").classList.add("blur_appear");
		});
		
		closeSeachBoxBtn.addEventListener("click", function() {
			document.querySelector(".search-qa .qa-container").innerHTML = "<h2 style='text-align:center;color:red;'>Empty!!!</h2>";
			document.querySelector(".search-qa").classList.remove("appear");
			document.querySelector(".blur").classList.remove("blur_appear");
		});
	}

});